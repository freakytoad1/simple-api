package main

import (
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
)

type Server struct {
	gingine *gin.Engine
}

type Stats struct {
	Single     time.Duration
	GoRoutines time.Duration
}

func NewServer() (*Server, error) {
	server := &Server{
		gingine: gin.Default(),
	}
	return server, nil
}

func (s *Server) setupRouter() {
	// Ping test
	s.gingine.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	// Simple GET
	s.gingine.GET("/apiCall", s.getSimpleJSON)

	// Goroutine vs single
	s.gingine.GET("/comparison", s.getComparison)
}

func (s *Server) getSimpleJSON(c *gin.Context) {
	host := c.Request.Host
	log.Printf("Request from host: %s", host)

	returnStruct := struct {
		name      string
		branch    string
		language  string
		Particles int
	}{
		name:      "Pikachu",
		branch:    "ECE",
		language:  "C++",
		Particles: 498,
	}
	c.JSON(http.StatusOK, returnStruct)
}

func hardFunc(iterations int, goroutine bool) time.Duration {
	start := time.Now()
	if goroutine {
		var wg sync.WaitGroup
		for i := 0; i < iterations; i++ {
			wg.Add(1)
			go doSomething()
			wg.Done()
		}
		wg.Wait()
	} else {
		for i := 0; i < iterations; i++ {
			doSomething()
		}
	}

	duration := time.Since(start)
	return duration
}

func doSomething() {
	for i := 0; i < 1000; i++ {
		_ = time.Now()
	}
}

func (s *Server) getComparison(c *gin.Context) {
	gr := hardFunc(1000, true)
	log.Printf("Go routine time: %s", gr)

	sing := hardFunc(1000, false)
	log.Printf("Single time: %s", sing)

	stats := &Stats{
		GoRoutines: gr,
		Single:     sing,
	}
	c.JSON(http.StatusOK, stats)
}

func main() {
	s, _ := NewServer()
	s.setupRouter()
	// Listen and Server in 0.0.0.0:8080
	s.gingine.Run(":8080")
}
